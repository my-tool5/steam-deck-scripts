#!/usr/bin/env bash

    if [ -d ~/.steam-deck-scripts/bin ]; then
        echo "~/.steam-deck-scripts/bin detected, backing up..."
        mkdir -p ~/.steam-deck-scripts/bin.old && mv ~/.steam-deck-scripts/bin/* ~/.steam-deck-scripts/bin.old/
        cp -r ./bin/* ~/.steam-deck-scripts/bin;
	# clear
    fi
    echo "Installing bin scripts..."
    mkdir -p ~/.steam-deck-scripts/bin && cp -r ./bin/* ~/.steam-deck-scripts/bin/;
	# clear
    SHELLNAME=$(echo $SHELL | grep -o '[^/]*$')
    case $SHELLNAME in
        bash)
            if [[ ":$PATH:" == *":$HOME/.steam-deck-scripts/bin:"* ]]; then
                echo "$HOME/.steam-deck-scripts/bin is already in your PATH(bash). Proceeding."
            else
                echo "Looks like $HOME/.steam-deck-scripts/bin is not on your PATH(bash), adding it now."
                echo "export PATH=\$PATH:\$HOME/.steam-deck-scripts/bin" >> $HOME/.bashrc
            fi
            ;;

        zsh)
            if [[ ":$PATH:" == *":$HOME/.steam-deck-scripts/bin:"* ]]; then
                echo "$HOME/.steam-deck-scripts/bin is already in your PATH(zsh). Proceeding."
            else
                echo "Looks like $HOME/.steam-deck-scripts/bin is not on your PATH(zsh), adding it now."
                echo "export PATH=\$PATH:\$HOME/.steam-deck-scripts/bin" >> $HOME/.zshrc
            fi
            ;;

        fish)
            echo "I see you use fish. shahab96 likes your choice."
            fish -c fish_add_path -P $HOME/.steam-deck-scripts/bin
            ;;

        *)
            echo "Please add: export PATH='\$PATH:$HOME/.steam-deck-scripts/bin' to your .bashrc or whatever shell you use."
            echo "If you know how to add stuff to shells other than bash, zsh and fish please help out here!"
    esac
