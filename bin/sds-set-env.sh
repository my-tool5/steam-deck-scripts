#!/usr/bin/env bash

# Establecemos las variables por defecto
_SRV=192.168.1.100
_ORIG=/no_directory
DST=$HOME/Games

# Comprobamos la existencia del directorio destino en el SteamDeck (/home/deck/Games)
# y si no existe, lo creamos
if ! [ -d ./"${DST}" ]; then
    mkdir -p "${DST}"
fi

# Dependiendo del argumento proporcionado, exportamos o bien la ruta origen
# o bien la variable NO_GAME=1
if [ "$1" = elden ]
then
    _ORIG=/games/Elden\ Ring
elif [ "$1" = ds ]
then
    _ORIG=/games2/Death\ Stranding
elif [ "$1" = scarlet ]
then
    _ORIG=/games/SCARLET\ NEXUS
elif [ "$1" = digimon ]
then
    _ORIG=/games/Digimon\ Survive
elif [ "$1" = neo ]
then
    unset NO_GAME
    _ORIG=/games/NEO\ The\ World\ Ends\ with\ You
else
    NO_GAME=1
    export NO_GAME
fi
