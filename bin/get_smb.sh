#!/usr/bin/env bash
# shellcheck source=/dev/null
. "$(which sds-set-env.sh)" "$1"
SRC=${_SRV}:${_ORIG}





# Si no nos han pasado argumento levantamos aviso.
if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit
fi

# Si el juego no está en la lista (sds-set-env.sh) levantamos aviso
# y si está, lo copiamos.
if [ -n "$NO_GAME" ] ; then
  echo "Game not found"
else
  echo "Hacemos smbclient de ""$_ORIG"
  smbclient -U someuser%somepassword "${SRC}" --directory "${DST}" -c 'put "test.deb"'
fi
