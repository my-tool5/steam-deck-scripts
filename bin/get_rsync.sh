#!/usr/bin/env bash
# shellcheck source=/dev/null
. "$(which sds-set-env.sh)" "$1"
SRC=rsync://${_SRV}${_ORIG}

# Definimos las funciones
function _get_rsync {
    rsync -avzhP "${SRC}" "${DST}"
}

# Si no nos han pasado argumento levantamos aviso.
if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit
fi

# Si el juego no está en la lista (sds-set-env.sh) levantamos aviso
# y si está, lo copiamos.
if [ -n "$NO_GAME" ] ; then
  echo "Game not found"
else
  echo "Hacemos rsync de ""$_ORIG"
  _get_rsync
fi
