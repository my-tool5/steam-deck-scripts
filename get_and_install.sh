#!/usr/bin/env bash

GIT_URL="https://gitlab.com"
GIT_GROUP="my-tool5"
GIT_PROJECT="steam-deck-scripts"

function _git_clone {
    git clone "$1"/"$2"/"$3"
}

if [ -d ./${GIT_PROJECT} ]; then
    cd ${GIT_PROJECT} || exit
    git pull
    sh install.sh
else
    _git_clone ${GIT_URL} ${GIT_GROUP} ${GIT_PROJECT}
    cd ${GIT_PROJECT} || exit
    sh install.sh
fi
