# steam-deck-scripts

Colección de scripts útiles para ser usados en SteamDeck.

## Instalación

Use el script de instalación [install.sh](https://gitlab.com/my-tool5/steam-deck-scripts/-/raw/main/install.sh) para instalar los scripts.
```bash
git clone https://gitlab.com/my-tool5/steam-deck-scripts
cd steam-deck-scripts
./install.sh
```
O instale con un único comando usando [get_and_install.sh](https://gitlab.com/my-tool5/steam-deck-scripts/-/raw/main/get_and_install.sh):
```bash
curl https://gitlab.com/my-tool5/steam-deck-scripts/-/raw/main/get_and_install.sh | sh
```

## Uso

Una vez instalado ejecutaremos los siguientes comandos dependiendo del juego que queramos descargar:

elden(Elden Ring)
```bash
get_rsync.sh elden
```
ds(Death Stranding)
```bash
get_rsync.sh ds
```
scarlet(Scarlet Nexus)
```bash
get_rsync.sh scarlet
```
digimon(Digimon Survive)
```bash
get_rsync.sh digimon
```
neo(NEO The World Ends with You)
```bash
get_rsync.sh neo
```
Una vez ejecutado el comando y terminada la transferencia tendremos los juegos instalados en ~/Games
